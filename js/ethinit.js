var debug = true

if (typeof web3 !== undefined) {
    web3 = new Web3(window.web3.currentProvider)
}
else {
    console.error('Browser not supported by Ethereum!')
    throw 'Browser not supported.'
}

var contract = web3.eth.contract(abi)

var login = contract.at('0x8547a8583c7059879102195166e05caf07539f4c')
if (debug) console.log(login)
