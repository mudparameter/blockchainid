// alexander peters wrote this

// MARK: check for existing transaction using cookie to provide consistency between browser sessions
const lastTxHash = Cookies.get('lastTxHash') // get prev recorded transaction hash from cookie
if (lastTxHash) { // if it exists,
    console.log('previous transaction from this browser:', lastTxHash) // print it, 
    web3.eth.getTransaction(lastTxHash, (err, res) => { // and attempt to get more into about it
        if (err) console.log(err) // print error if it errors out
        else if (!res.blockNumber) {
            $('#loadingGif').show() // if the tx has no block number b/c it has not been mined yet, show the loading gif
            $('#status').text('request pending with txHash: ' + lastTxHash) // AND display the pending txHash
        }
    })
}

// MARK: creation
$('#addbutton').click(() => { // when the add user button is clicked
    login.add_user(web3.fromAscii($('#pass').val()), (err, res) => { // encode and pass value that is present in field
        if (err) console.error(err) // print err if it errors out
        else { // we were successful in initiating a user creation request
            Cookies.set('lastTxHash', res, { expires: 1 }) // set the cookie to the txHash for continuity in case the user decides to close the tab
            $('#loadingGif').show() // show the loading gif
            $('#status').text('request pending with txHash: ' + res) // display the pending txHash
        }
    })
})

// MARK: creation event handling
var added = login.added() // added event

added.watch((err, res) => { // clean up once user added event is fired
    $('#loadingGif').hide() // hide loading gif
    if (err) console.error(err) // print error if it errors out
    else $('#status').text('success! your new hashed password: ' + res.args.pass) // set display to hashed pw
})

// MARK: validation
$('#submit').click(() => { // when the user clicks the login button
    login.login(web3.fromAscii($('#pass').val()), (err, res) => { // attempt to log in with pw in field
        if (err) console.log(err) // print error if it errors out
        else {
            if (res) $('#status').text('password correct') // if a response is received, pass is correct. 
            else $('#status').text('your pass was incorrect OR you do not have an account') // if not, it was not. contract returns a bool
        }
    }) 
})

// MARK: testing
$('#getpass').click(() => { // when user clicks get pw button
    login.get_pass((err, res) => { // attempt to get the user's hashed pw from the blockchain
        if (err) console.log(err) // print err if it errors out
        else $('#status').text('success. your hashed password: ' + res) // display what we get
    }) 
})
