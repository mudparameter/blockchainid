pragma solidity ^0.4.0;

contract Owned
{
    address owner;
    
    constructor() public 
    {
        owner = msg.sender;
    }
    
    modifier onlyOwner
    {
        require(msg.sender == owner);
        _;
    }
}

contract Login is Owned
{
    mapping (address => bytes32) authmap;
    
    event added (address addr, bytes32 pass);
    
    function add_user(bytes32 pass) onlyOwner public returns (bool)
    {
        authmap[msg.sender] = keccak256(abi.encodePacked(pass));
        emit added(msg.sender, authmap[msg.sender]);
    }
    
    function login(bytes32 pass) public view returns (bool)
    {
        if (authmap[msg.sender] == 0) return false;
        if (keccak256(abi.encodePacked(pass)) == authmap[msg.sender]) return true;
        return false;
    }
    
    function get_pass() public view returns (bytes32)
    {
        return authmap[msg.sender];
    }
    
    function kill() onlyOwner public
    {
        selfdestruct(owner);
    }
}
